#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 14:48:44 2020

@author: gustavo

LID DRIVEN CAVITY
"""


import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter




rho = np.zeros((100, 100)) 
u = np.zeros((100, 100)) 
v = np.zeros((100, 100)) 

feq = np.zeros((100, 100, 9))
omega = 2.0/7.0
f = np.zeros((100, 100, 9))
f_linha = np.zeros((100, 100, 9)) #para atualizar o vetor na propagação sem sobrepor valores

w = np.array([4/9 , 1/9, 1/9, 1/9, 1/9, 1/36, 1/36, 1/36, 1/36, 1/36])
cx = np.array([0, 1, 0, -1, 0, 1, -1, -1, 1])
cy = np.array([0, 0, 1, 0, -1, 1, 1, -1, -1])

n = 100
m = 100

uo = 0.1
sum_velo = 0.0
rhoo = 5.00
alpha = 0.01 #viscosidade cinemática
# Re = uo * 100 / alpha

for x in range(0, 100):
    for y in range(0, 100):
        rho[x, y] = rhoo 
        
for i in range(1, 99):
    u[i, 99] = uo

for t in range(400):
    for i in range(0, n):
        for j in range(0, n):         
            t1 = u[i, j] * u[i, j] + v[i, j] * v[i,j]
            for k in range(0, 9):
                t2 = u[i, j] * cx[k] + v[i, j] * cy[k]
                feq[i, j, k] = rho[i,j] * w[k] * (1.0 + 3.0 * t2 + 4.5 * t2 * t2 - 1.5 * t1)
                f[i, j, k] = omega * feq[i, j, k] + (1.0 - omega) * f[i, j, k]
        
    for y in range(99, 0, -1):
    	for x in range(0, 99):
    		f_linha[x, y, 2] = f[x, y-1, 2] 
    		f_linha[x, y, 6] = f[x+1, y-1, 6] 
    
    for y in range(99, 0, -1):
    	for x in range(99, 0, -1):
    		f_linha[x, y, 1] = f[x-1, y, 1]
    		f_linha[x, y, 5] = f[x-1, y-1, 5]
    
    for y in range(0, 99):
    	for x in range(99, 0, -1):
    		f_linha[x, y, 4] = f[x, y+1, 4]
    		f_linha[x, y, 8] = f[x-1, y+1, 8]
     
    for y in range(0, 99):
    	for x in range(0, 99):
    		f_linha[x, y, 3] = f[x+1, y, 3]
    		f_linha[x, y, 7] = f[x+1, y+1, 7]
    
    
    for p in range(0,100):
    	#bounce back on west boundary
    	f_linha[0, p, 1] = f[0, p, 3]
    	f_linha[0, p, 5] = f[0, p, 7]
    	f_linha[0, p, 8] = f[0, p, 6]

   
    	#bounce back on east boundary
    	f_linha[99, p, 6] = f[99, p, 8]
    	f_linha[99, p, 3] = f[99, p, 1]
    	f_linha[99, p, 7] = f[99, p, 5]
        
    	#bounce back on south boundary
    	f_linha[p, 0, 2] = f[p, 0, 4]
    	f_linha[p, 0, 6] = f[p, 0, 8]
    	f_linha[p, 0, 5] = f[p, 0, 7]

    #movin lid, north boundary    
    for i in range(1, n-1):
        rho_n = f[i, m-1, 0] +f[i, m-1, 1] + f[i, m-1, 3] + 2 * ( f[i, m-1, 2] + f[i, m-1, 6] + f[i, m-1, 5] )
        f_linha[i, m-1, 4] = f[i, m-1, 2]
        f_linha[i, m-1, 8] = f[i, m-1, 6] + rho_n * uo / 6.0
        f_linha[i, m-1, 7] = f[i, m-1, 5] - rho_n * uo / 6.0
        
    for i in range(0, 100):
        for j in range(0, 100):
            rho[i, j] = sum(f[x, y, : ])
        rho[i, 99] = f[i, m-1, 0] +f[i, m-1, 1] + f[i, m-1, 3] + 2 * ( f[i, m-1, 2] + f[i, m-1, 6] + f[i, m-1, 5] )
        
    tmp = f
    f = f_linha
    f_linha = tmp 
    
X = np.arange(0, 100, 1)
Y = np.arange(0, 100, 1)
X, Y = np.meshgrid(X, Y)
    
fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(X, Y, rho, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

ax.set_zlim(-0.1, 5.00)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()