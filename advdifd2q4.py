#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 22 14:33:26 2020

@author: gustavo
"""

import numpy as np
import matplotlib.pyplot as plt



T = np.zeros((100, 100)) 
feq = np.zeros((100, 100, 4))
omega = 2.0/5.0
f = np.zeros((100, 100, 4))
f_linha = np.zeros((100, 100, 4)) #para atualizar o vetor na propagação sem sobrepor valores
u = 0.1
v = 0.2

for t in range(200):
    
    for x in range(0,100):
        for y in range(0,100):
            T[x, y] = sum(f[x, y, :])
            feq[x, y, 0] = T[x, y] * 0.25 * (1.0 + (2.0 * u))
            feq[x, y, 1] = T[x, y] * 0.25 * (1.0 - (2.0 * u))
            feq[x, y, 2] = T[x, y] * 0.25 * (1.0 + (2.0 * v))
            feq[x, y, 3] = T[x, y] * 0.25 * (1.0 - (2.0 * v))
            for k in range(0, 4):
                f[x, y, k] = f[x, y, k]*(1 - omega) + omega * feq[x, y, k] #colisão
        
    for y in range(0, 99):
            for x in range(1, 100):
                    f_linha[99-x, y, 0] = f[98-x, y, 0] 
                    f_linha[x-1, y, 1] = f[x, y, 1]
    
    for x in range(0, 99):
            for y in range(1, 100):
                    f_linha[x, 99-y, 2] = f[x, 98-y, 2]
                    f_linha[x, y-1, 3] = f[x, y, 3]
    
    for p in range(0, 100): 
            f_linha[0, p, 0] = 0.5 - f_linha[0, p, 1]
            f_linha[0, p, 2] = 0.5 - f_linha[0, p, 3]
            
            f_linha[99, p, 0] = 0.0
            f_linha[99, p, 1] = 0.0
            f_linha[99, p, 2] = 0.0
            f_linha[99, p, 3] = 0.0
    
            f_linha[p, 99, 0] = 0.0
            f_linha[p, 99, 1] = 0.0
            f_linha[p, 99, 2] = 0.0
            f_linha[p, 99, 3] = 0.0
    
            f_linha[p, 0, 0] = f[p, 1, 0]
            f_linha[p, 0, 1] = f[p, 1, 1]
            f_linha[p, 0, 2] = f[p, 1, 2]
            f_linha[p, 0, 3] = f[p, 1, 3]       

    tmp = f
    f = f_linha
    f_linha = tmp 
    
Y = T[ : , 50]
X = np.arange(0, 100, 1)
    
plt.plot(X, Y)
plt.show()
