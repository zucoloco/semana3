#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 13:07:24 2020

@author: gustavo
"""

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

T = np.zeros((100, 100)) 
feq = np.zeros((100, 100, 9))
omega = 2.0/7.0
f = np.zeros((100, 100, 9))
f_linha = np.zeros((100, 100, 9)) #para atualizar o vetor na propagação sem sobrepor valores
w = np.array([4/9 , 1/9, 1/9, 1/9, 1/9, 1/36, 1/36, 1/36, 1/36, 1/36])
T_wall = 1.0
v = 0.4
u = 0.1 


for t in range(400):
    for x in range(0,100):
        for y in range(0,100):
            T[x, y] = sum(f[x, y, : ])
            feq[x, y, 0] = T[x, y] * w[0]
            feq[x, y, 1] = T[x, y] * w[1] * (1 + 3 * u)
            feq[x, y, 2] = T[x, y] * w[2] * (1 + 3 * v)
            feq[x, y, 3] = T[x, y] * w[3] * (1 + 3 * (-u))
            feq[x, y, 4] = T[x, y] * w[4] * (1 + 3 * (-v))
            feq[x, y, 5] = T[x, y] * w[5] * (1 + 3 * (u+v))
            feq[x, y, 6] = T[x, y] * w[6] * (1 + 3 * (v-u))
            feq[x, y, 7] = T[x, y] * w[7] * (1 + 3 * (-u-v))
            feq[x, y, 8] = T[x, y] * w[8] * (1 + 3 * (u-v))
            
            for k in range(0, 9):
                f[x, y, k] = f[x, y, k]*(1 - omega) + omega * feq[x, y, k] #colisão
        
    for y in range(99, 0, -1):
    	for x in range(0, 99):
    		f_linha[x, y, 2] = f[x, y-1, 2] 
    		f_linha[x, y, 6] = f[x+1, y-1, 6] 
    
    for y in range(99, 0, -1):
    	for x in range(99, 0, -1):
    		f_linha[x, y, 1] = f[x-1, y, 1]
    		f_linha[x, y, 5] = f[x-1, y-1, 5]
    
    for y in range(0, 99):
    	for x in range(99, 0, -1):
    		f_linha[x, y, 4] = f[x, y+1, 4]
    		f_linha[x, y, 8] = f[x-1, y+1, 8]
     
    for y in range(0, 99):
    	for x in range(0, 99):
    		f_linha[x, y, 3] = f[x+1, y, 3]
    		f_linha[x, y, 7] = f[x+1, y+1, 7]
    
    for p in range(0,100):
    	#left boundary
    	f_linha[0, p, 1] = w[1] * T_wall + w[3] * T_wall - f_linha[0, p, 3]
    	f_linha[0, p, 5] = w[5] * T_wall + w[7] * T_wall - f_linha[0, p, 7]
    	f_linha[0, p, 8] = w[8] * T_wall + w[6] * T_wall - f_linha[0, p, 6]
    
    	#right boundary
    	f_linha[99, p, 6] = -f[99, p, 8]
    	f_linha[99, p, 3] = -f[99, p, 1]
    	f_linha[99, p, 7] = -f[99, p, 5]
    	f_linha[99, p, 2] = -f[99, p, 4]
    	f_linha[99, p, 0] = 0.0
    
    	#top boundary
    	f_linha[p, 99, 8] = -f[p, 99, 6]
    	f_linha[p, 99, 7] = -f[p, 99, 5]
    	f_linha[p, 99, 4] = -f[p, 99, 2]
    	f_linha[p, 99, 1] = -f[p, 99, 3]
    	f_linha[p, 99, 0] = 0.0
    
    	#bottom boundary
    	f_linha[p, 0, 2] = -f[p, 0, 4]
    	f_linha[p, 0, 6] = -f[p, 0, 8]
    	f_linha[p, 0, 5] = -f[p, 0, 7]
    	f_linha[p, 0, 1] = -f[p, 0, 3]
    	f_linha[p, 0, 0] = 0.0
        
    tmp = f
    f = f_linha
    f_linha = tmp 
    
X = np.arange(0, 100, 1)
Y = np.arange(0, 100, 1)
X, Y = np.meshgrid(X, Y)
    
fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(X, Y, T, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

ax.set_zlim(-0.1, 1.01)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()